<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class tarjeta_model extends CI_Model{//Inicio de la clase CI_model

//metodo de registrar cliente en la base de datos
	public function set_tarjeta($datos){
		//se obtienen los datos del controlador y se insertan en los campos
		$this->db->set('n_tarjeta', $datos['numero']);
		$this->db->set('fecha_t', $datos['fecha']);
		$this->db->set('titular', $datos['titular']);
		$this->db->set('codigo_cvv', $datos['codigo']);
		$this->db->insert('pago');
		if ($this->db->affected_rows()>0) {
			return "success";
		}
	}//fin del metodo registrar

	public function tarjeta_cliente($datos,$id){
		$this->db->set('tarjeta', $datos['numero']);
		$this->db->where('id',$id);
		$this->db->update('cliente');

		if ($this->db->affected_rows()>0) {
			$datos_correo = array('tarjeta'=> True);

			$this->session->set_userdata($datos_correo);
		}
	}


}//fin de la clase CI_Model




