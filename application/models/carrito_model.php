<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class carrito_model extends CI_model {


	
	public function guardar(){

		$cliente=$this->session->userdata('id');
		$total=$this->session->userdata('total');
		$fecha = date("y-m-d");

		foreach ($this->cart->contents() AS $key =>  $v) {
			
			$this->db->set('cliente_id',$cliente);
			$this->db->set('producto_id',$v["id"]);
			$this->db->set('fecha',$fecha);
			$this->db->set('cantidad',$v["qty"]);
			$this->db->set('precio_pro',$v["price"]);
			$this->db->set('total',$total);
			$this->db->insert('factura');

			
			$this->cart->destroy();
			
		}
		
	}
	
	public function validar($datos){
		//validamos que la targeta y codigo cvv existan
		$this->db->where('n_tarjeta',$datos['numero']);
		$this->db->where('codigo_cvv',$datos['codigo']);
		$exe = $this->db->get('pago');

		if($this->db->affected_rows()>0){
			return true;
		}else{
			return false;
		}

		
	}

	public function get_pago(){
		$exe = $this->db->get('pago');
		return $exe->result();
	}
	
}


