	<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class inicio_model extends CI_model {
//metodo para traer todas las categorias
		public function categorias(){

			$categorias = $this->db->get('categoria');

			return $categorias->result();

		}
//metodo para traer  los primeros 8 produstos 
		public function productos(){
			$productos = $this->db->get('producto', 8);

			return $productos->result();
		}
//metodo que ignora los primeros 8 productos y trae los demas
		public function produ(){
			$query = $this->db->query("SELECT * FROM `producto` LIMIT 8, 30");
			return $query->result();
		}
//trae todos los detalles del producto
		public function detallesProducto($id){	
			$this->db->select('p.id, img, nombre_producto, descrip_pro, precio, c.nombre_cate');
			$this->db->from('producto p');
			$this->db->join('categoria c','c.id = p.categoria_id');
			$this->db->where('p.id',$id);
			$detalles = $this->db->get();
			return $detalles->row();
		}



	}