<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class registro_model extends CI_Model{//inicio de la clase registro_model

//metodo de registrar cliente en la base de datos
	public function set_registro($datos){
		$this->db->set('nombre', $datos['nombre']);
		$this->db->set('apellido', $datos['apellido']);
		$this->db->set('fecha_nacimiento', $datos['fecha']);
		$this->db->set('genero', $datos['genero']);
		$this->db->set('direccion', $datos['direccion']);
		$this->db->set('correo', $datos['correo']);
		$this->db->set('clave', $datos['clave']);
		$this->db->insert('cliente');
	}//fin del metodo registrar

}//fin de la clase registro_model