<!DOCTYPE html>
<html>
<head>
	<title>Registro</title>
	<link href="https://stackpath.bootstrapcdn.com/bootswatch/4.4.1/cerulean/bootstrap.min.css" rel="stylesheet" integrity="sha384-LV/SIoc08vbV9CCeAwiz7RJZMI5YntsH8rGov0Y2nysmepqMWVvJqds6y0RaxIXT" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('bootstrap/css/bootstrap.min.css.map'); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('bootstrap/iconos/iconos/iconos.css'); ?>">
	<script src="https://kit.fontawesome.com/11f5858a75.js" crossorigin="anonymous"></script> 
	<script src="<?php echo base_url('bootstrap/js/registro.js'); ?>"></script>
</head>
<body style=" background: url(<?php echo base_url().'p_login/images/compra2.jpg';?>);
background-size: cover;
background-position: center center;
background-repeat: no-repeat;
background-attachment: fixed;
background-size: cover;
background-color: black;">


<div class="container col-md-4" style="font-size: 100%; margin-top: 10px; border-radius: 10px; background: mintcream; padding: 15px; border: solid; border-color: #191A1A; box-shadow: 5px 6px 7px #191A1A;">
	<!-- formulario llama al metodo ingresar del controlador registrar_controller -->
	<form autocomplete="off" action="<?php echo base_url('registro_controller/ingresar'); ?>" method="POST"  onsubmit="return validarFormulario()">
		<div>
			<input type="hidden" name="">
		</div>
		<div align="center">
			<label style="font-size: 150%; font-family: arial; font-weight: 900;" class="text-info">¡¡¡Crea una cuenta!!!</label>
		</div>
		<br>

		<div class="form-group ">
			<span style="font-size: 100%; font-family: arial; font-weight: 700; ">Nombres</span>
			<div class="input-group-prepend">
				<div class="input-group-text"><i class="fas fa-user-edit"></i></div>
				<input  type="text" name="nombre" id="nombre" class="form-control"placeholder="Digite su nombre...">
			</div>
			
		</div>

		<div class="form-group ">
			<span style="font-size: 100%; font-family: arial; font-weight: 700; ">Apellidos</span>
			<div class="input-group-prepend">
				<div class="input-group-text"><i class="fas fa-user-edit"></i></div>
				<input type="text" name="apellido" id="apellido" class="form-control" placeholder="Digite su apellido...">
			</div>
		</div>
		<div class="form-group ">
			<span style="font-size: 100%; font-family: arial; font-weight: 700; ">Fecha de nacimiento</span>
			<div class="input-group-prepend">
				<div class="input-group-text"><i class="fas fa-calendar-alt"></i></div>
				<input type="date" name="fecha" id="fecha" class="form-control">
			</div>
		</div>

		<div class="form-group ">
			<span style="font-size: 100%; font-family: arial; font-weight: 700; ">Direccion</span>
			<div class="input-group-prepend">
				<div class="input-group-text"><i class="fas fa-map-marker-alt"></i></div>
				<input  type="text" name="direccion" id="direccion" class="form-control" placeholder="Digite su direccion...">
			</div>
		</div>
		<div class="form-group ">
			<span style="font-size: 100%; font-family: arial; font-weight: 700; ">Correo</span>
			<div class="input-group-prepend">
				<div class="input-group-text"><i class="fas fa-envelope"></i></div>
				<input  type="text" name="correo" id="correo" class="form-control" placeholder="Digite su correo...">
			</div>
		</div>
		<div class="form-group ">
			<span style="font-size: 100%; font-family: arial; font-weight: 700; ">Contraseña</span>
			<div class="input-group-prepend">
				<div class="input-group-text"><i class="fas fa-lock"></i></div>
				<input class="form-control" type="password" name="clave" id="clave" placeholder="Digite su contraseña...">
			</div>
		</div>
		<div class="form-group ">
			<span style="font-size: 100%; font-family: arial; font-weight: 700; ">Confirmar contraseña</span>
			<div class="input-group-prepend">
				<div class="input-group-text"><i class="fas fa-unlock"></i></div>
				<input type="password" name="clave2" id="clave2" class="form-control" placeholder="Confirmar contrasea...">
			</div>
		</div>
		
		<label style="font-size: 100%; font-family: arial; font-weight: 700; margin-top: 10px;">Seleccione un genero</label>

		<div class="form-check">
			<input type="radio"  name="genero" id="genero" value="Masculino">
			<label class="form-check-label">
				Masculino
			</label>
		</div>
		<div class="form-check">
			<input type="radio"  name="genero" id="genero" value="Femenino">
			<label class="form-check-label">
				Femenino
			</label>
		</div>

		<div>
			<input type="submit" value="CREAR CUENTA" class="btn btn-outline-info" style="font-size: 100%; height: 70%; width: 100%; margin-top: 10px;">
		</div>	
	</form>
</div>

<!-- validacion de verifica que las claves sean iguales -->
<script type="text/javascript">
	window.onload= function(){
		document.getElementById('clave').onchange = verificar;
		document.getElementById('clave2').onchange = verificar;
	}

	function verificar(){
		var newClave = document.getElementById('clave').value;
		var clave = document.getElementById('clave2').value;

		if(newClave!=clave){
			document.getElementById('clave2').setCustomValidity('las claves no coinciden');
		}else{
			document.getElementById('clave2').setCustomValidity('');
		}

	}

</script>
</body>
</html>