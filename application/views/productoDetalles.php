<body>
	<br>

	<div class="container-fluid">
		
		<div style="border-radius: 10px;" class="card-header text-center"><h2 class="titulo-detalles">Este producto y más relacionados los encuentras en categoria de <?=$detalle->nombre_cate ?> </h2></div>
		<form action="<?php echo base_url().'addcarrito_controller/carrito/'.$detalle->id; ?>" method="post" onsubmit="return validarform()">
			<input type="hidden" name="id" value="<?=$detalle->id  ?>">
			<input type="hidden" name="nombre" value="<?=$detalle->nombre_producto  ?>" >
			<input type="hidden" name="precio" value="<?=$detalle->precio  ?>" >
			<div class="row">

				<div class="col-md-6">
					<div class="card border-light mb-3" style="max-width: 45rem;">
						<div class="card-body">
							<h2 class="card-title text-dark"><b><?=$detalle->nombre_producto ?></b></h2>
							<br>
							<img style="border-radius: 10px;" width="350" height="400" src="<?php echo base_url().'img/productos/'.$detalle->img; ?>">


						</div>
					</div>
				</div>

				<div class="col-md-6">

					<div class="card border-light mb-3" style="max-width: 45rem;">

						<div class="card-body">
							<br><br>
							<p class="card-text"><?=$detalle->descrip_pro ?>.</p>

							<div class="row">

								<input type="text" id="precio"  readonly="" value="Precio: $<?=$detalle->precio ?>" class="form-control card-header col text-center">
								<input type="number" min="0" id="cantidad" name="cantidad"  placeholder="Cantidad" class="form-control card-header col text-center">
								<button type="submit" class="btn btn-info text-center">Agregar a carrito <i class="fas fa-cart-plus"></i></button>


							</div>

						</div>



					</div>

				</div>

			</div>
		</form>
	</div>
</body>

<script type="text/javascript">
	function validarform(){
		var cantidad = document.getElementById('cantidad').value;

		if(cantidad.length == ""){
			document.getElementById("cantidad").style.borderColor = 'red';
			document.getElementById("cantidad").style.boxShadow = '0 0 5px red';
			document.getElementById("cantidad").placeholder = "Campo requerido!!!";
			return false;
		}else{
			document.getElementById("cantidad").style.borderColor = 'green';
			document.getElementById("cantidad").style.boxShadow = '0 0 5px green';
		}

		return true;
	}
</script>