<title>Tarjeta de credito</title>
<head> <link rel="stylesheet" type="text/css" href="<?php echo base_url('bootstrap/css/bootstrap.min.css'); ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('bootstrap/css/bootstrap.min.css.map'); ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('bootstrap/iconos/iconos/iconos.css'); ?>">
  <script src="https://kit.fontawesome.com/11f5858a75.js" crossorigin="anonymous"></script> 
  <script src="<?php echo base_url('bootstrap/js/jquery.min.js'); ?>"></script>
  <script src="<?php echo base_url('bootstrap/js/tarjeta.js'); ?>"></script>
  <script src="<?php echo base_url('bootstrap/js/jquery.mask.min.js') ?>"></script>
     <script type="text/javascript" src="<?php echo base_url('props/bootstrap/js/bootstrap.min.js')?>"></script>
</head>
<body class="backgroundbody" background="../props/imagenes/carrito.jpg" style="width: 100% no-repeat; background-size:100%" >
	<div class="container col-md-10 my-3 ">
		<span class="d-block p-2 bg-success text-white" style="color: white">Productos en carrito</span>


		<?php if(!$this->session->userdata('logueado')){ ?>
			<span style="color" class="badge badge-pill badge-danger"><i class="fa fa-warning (alias)">&nbsp</i>Debe iniciar sesion para poder registrar la compra</span>
		<?php } ?>

		
		<hr>
   <?php if($this->session->userdata('logueado')){ ?>
    <table style="color: white" class="table table-dark table-striped table-hover table-sm">
     <tr>
      <th width="40%">Nombre Producto</th>
      <th width="10%">Cantidad</th>
      <th width="20%">Precio</th>
      <th width="15%">Total</th>
      <th width="5%">Quitar</th>
    </tr>

    <?php

    if(isset($_GET['quitar'])){
      foreach ($this->cart->contents() AS $key => $value){
       if($value['rowid'] == $_GET['quitar']){
        $borrar = array(
         'rowid' => $_GET['quitar'],
         'qty'   => 0
       );
        $this->cart->update($borrar);
        echo '<script>alert("El Producto Fue Eliminado!");</script>';
        echo '<script>window.location.href = "../carrito_controller/index" ;</script>';
      }
    }
  }  


  if(!empty($this->cart->contents())){
    $total = 0;
    foreach ($this->cart->contents() AS $key => $value){?>

     <tr>
      <td><?= $value['name'];?></td>
      <td><?= $value["qty"];?></td>
      <td>$<?= $this->cart->format_number($value["price"]);?></td>
      <td>$<?php echo number_format($value["qty"] * $value["price"],2);?></td>
      <td><a class="btn btn-danger" href="?quitar=<?= $value["rowid"];?>">Quitar</a></td>
    </tr>
    <?php
    $data = array('total' => $total =$this->cart->format_number($this->cart->total())); 
  }
  $this->session->set_userdata($data);
  ?>
  <tr style="color: white">
   <td  colspan="3" align="right"><strong>Total</strong></td>
   <td>$<strong style="color:green;"><?=$this->cart->format_number($this->cart->total());?></strong></td>
   <td></td>
 </tr>
 <?php

}else{

  ?>
  <tr style="color: white">
   <td colspan="4" style="color: red" align="center"><strong>No hay Producto Agregado!</strong></td>
 </tr>
 <?php

}

?>

</table>

<?php $tarjeta=$this->session->userdata('tarjeta'); ?>

<?php if ($tarjeta == 0) { ?>
  <table>   
    <!-- Button trigger modal -->
    <div class="row">             
      <button type="button" class="btn btn-primary btn-sm justify-content-right" data-toggle="modal" data-target="#exampleModal"><i class='fa fa-th-list'></i>
        agregar tarjeta y comprar
      </button>
    </div>     



    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h3 class="modal-title" id="exampleModalLabel">Nueva tarjeta</h3>

          </div>
          <div class="modal-body">
            <div class="container col-md-8" style="font-size: 100%; margin-top: 8%; border-radius: 10px; background: white; padding: 15px; border: solid; border-color: #191A1A; box-shadow: 5px 6px 7px #191A1A;">
              <!-- formulario llama al metodo ingresar del controlador tarjeta_controller -->
              <form action="<?php echo base_url('tarjeta_controller/ingresar'); ?>" method="POST" onsubmit="return validarFormulario()">
                <div>
                  <input type="hidden" name="">
                </div>
                <div align="center">
                  <label style="font-size: 150%; font-family: arial; font-weight: 900;">¡¡¡Ingrese una tarjeta de credito!!!</label>
                </div>
                <br>

                <div class="form-group ">
                  <span style="font-size: 100%; font-family: arial; font-weight: 700; ">Numero de tarjeta</span>
                  <div class="input-group-prepend">
                    <div class="input-group-text"><i class="fas fa-credit-card"></i></div>
                    <input  type="text" name="numero" id="numero" class="form-control">
                  </div>
                </div>

                <div class="form-group ">
                  <span style="font-size: 100%; font-family: arial; font-weight: 700; ">Fecha de tarjeta</span>
                  <div class="input-group-prepend">
                    <div class="input-group-text"><i class="fas fa-calendar-alt"></i></div>
                    <input type="text" name="fecha" id="fecha" class="form-control">
                  </div>
                </div>

                <div class="form-group ">
                  <span style="font-size: 100%; font-family: arial; font-weight: 700; ">Titular</span>
                  <div class="input-group-prepend">
                    <div class="input-group-text"><i class="fas fa-credit-card"></i></div>
                    <input  type="text" name="titular" id="titular" class="form-control" placeholder="Digite su titular...">
                  </div>
                </div>
                <div class="form-group ">
                  <span style="font-size: 100%; font-family: arial; font-weight: 700; ">Codigo CVV</span>
                  <div class="input-group-prepend">
                    <div class="input-group-text"><i class="fas fa-credit-card"></i></div>
                    <input  type="text" name="codigo" id="codigo" class="form-control">
                  </div>
                </div>





                <div>

                  <input type="submit" value="guardar" class="btn btn-outline-primary" style="font-size: 100%; height: 70%; width: 100%; margin-top: 10px;">
                </div>



              </form>
            </div>
            <div class="row">
              <div class="col text-right">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
              </div>
            </div>



          </div>
        </div>
      </div>

      <br>
    </table>

  <?php }else{ ?>

    <table>   
      <!-- Button trigger modal -->
      <div class="row">             
        <button type="button" class="btn btn-primary btn-sm justify-content-right" data-toggle="modal" data-target="#exampleModal"><i class='fa fa-th-list'></i>
          Verificar tarjeta y comprar
        </button>
      </div>    

      <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h3 class="modal-title" id="exampleModalLabel">Verificasion de tarjeta</h3>

            </div>
            <div class="modal-body">
              <div class="container col-md-8" style="font-size: 100%; margin-top: 8%; border-radius: 10px; background: white; padding: 15px; border: solid; border-color: #191A1A; box-shadow: 5px 6px 7px #191A1A;">
                <!-- formulario llama al metodo ingresar del controlador tarjeta_controller -->
                <form action="<?php echo base_url('carrito_controller/verificar'); ?>" method="POST" onsubmit="return validarFormulario()">
                  <div>
                    <input type="hidden" name="">
                  </div>
               <!--  <div align="center">
                    <label style="font-size: 150%; font-family: arial; font-weight: 900;">¡¡¡Ingrese una tarjeta de credito!!!</label>
                  </div> -->
                  <br>

                  <div class="form-group ">
                    <span style="font-size: 100%; font-family: arial; font-weight: 700; ">Numero de tarjeta</span>
                    <div class="input-group-prepend">
                      <div class="input-group-text"><i class="fas fa-credit-card"></i></div>
                      <input  type="text" name="numero" id="numero" class="form-control">
                    </div>
                  </div>


                  <div class="form-group ">
                    <span style="font-size: 100%; font-family: arial; font-weight: 700; ">Codigo CVV</span>
                    <div class="input-group-prepend">
                      <div class="input-group-text"><i class="fas fa-credit-card"></i></div>
                      <input  type="text" name="codigo" id="codigo" class="form-control">
                    </div>
                  </div>




                  <div>

                    <input type="submit" value="Verificar y comprar" class="btn btn-outline-primary" style="font-size: 100%; height: 70%; width: 100%; margin-top: 10px;">
                  </div>



                </form>
              </div>
              <div class="row">
                <div class="col text-right">
                 <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
               </div>
             </div>

           </div>

         </div>
       </div>
     </div>

     <br>
   </table>

     <!--    <form action="<?php //echo base_url('carrito_controller/comprar'); ?>" method="POST">
            <div style="color: white" class="row">




               <input type="submit" name="accion" value="Comprar" class="btn btn-sm my-2 my-sm-0 btn-success">
           </div>
         </form> -->



       <?php }

     } ?>

    

   </div>	



 
   <script type="text/javascript">

    $(document).ready(function(){
        //FORMATO DE MASCARAS
        $('#numero').mask('0000-0000-0000-0000', {placeholder: '0000-0000-0000-0000'});
        $('#codigo').mask('000', {placeholder: '000'});

        $('#fecha').mask('00/00', {placeholder: '00/00'});
      });
    </script>
<?php $this->load->view('alerts/alertscarrito') ?>
      <!-- Fin de Alerta  -->
  </body>




