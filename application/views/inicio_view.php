<body>
  <div>
    <div id="carouselExample1" class="carousel slide z-depth-1-half" data-ride="carousel">
      <div class="carousel-inner">
        <div class="carousel-item active">
          <img class="d-block w-100" height="360" src="<?php echo base_url().'img/bg2.jpg'; ?>" alt="First slide">
          <div class="carousel-caption d-none d-md-block">
            <h4 class="titulo-carousel text-white"><b>Lorem ipsum dolor sit amet.</b></h4>
            <p class="p-carousel">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam, repellendus.</p>
            <button type="button" class="boton btn btn-outline-light">Descubre más  <i class="fas fa-arrow-right"></i></button>
          </div>
        </div>
        <div class="carousel-item">
          <img class="d-block w-100" height="360" src="<?php echo base_url().'img/bg3.jpg'; ?>" alt="Second slide">
          <div class="carousel-caption d-none d-md-block">
            <h4 class="titulo-carousel text-white"><b>Lorem ipsum dolor sit amet.</b></h4>
            <p class="p-carousel">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam, repellendus.</p>
            <button type="button" class="boton btn btn-outline-light">Descubre más  <i class="fas fa-arrow-right"></i></button>
          </div>
        </div>
        <div class="carousel-item">
          <img class="d-block w-100" height="360" src="<?php echo base_url().'img/bg1.jpg'; ?>" alt="Third slide">
          <div class="carousel-caption d-none d-md-block">
            <h4 class="titulo-carousel text-white"><b>Lorem ipsum dolor sit amet.</b></h4>
            <p class="p-carousel">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam, repellendus.</p>
            <button type="button" class="boton btn btn-outline-light">Descubre más  <i class="fas fa-arrow-right"></i></button>
          </div>
        </div>
      </div>
      <a class="carousel-control-prev" href="#carouselExample1" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselExample1" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>


  </div>

  
  <br><br>
<!--
  <div class="col my-4">
      <div class="card border-light mb-3" style="max-width: 30rem;">
        <div class="card-header text-center"><?= $productos->nombre_producto ?></div>
        <div class="card-body">
          
          <a href="<?php echo base_url().'inicio/verProducto/'.$productos->id; ?>"><img style="border-radius: 10px;" width="200" height="215" src="<?php echo base_url().'img/productos/'.$productos->img; ?>"></a>
          

          <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
          <p class="precio">$<?= $productos->precio ?></p>
          <a href="<?php echo base_url().'inicio/verProducto/'.$productos->id; ?> "><button type="button" class="btn btn-block btn-info">Agregar a carrito <i class="fas fa-cart-plus"></i></button></a>

        </div>
      </div>  
    </div>
  -->

  <div class="container">

  	<p class="producto">PRODUCTOS DESTACADOS</p>


    <div class="row">


      <?php foreach ($producto as $productos) {  ?>


       <div class="col my-4">
        <div class="card border-light mb-3" style="max-width: 30rem;">

         <div class="card-body">

          <a href="<?php echo base_url().'inicio/verProducto/'.$productos->id; ?>"><img style="border-radius: 10px;" width="200" height="215" src="<?php echo base_url().'img/productos/'.$productos->img; ?>"></a>
          <br><br>
          <div class="text-center"><b><?= $productos->nombre_producto ?></b></div>
          <br>
          <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
          <p class="precio">$<?= $productos->precio ?></p>
          <a href="<?php echo base_url().'inicio/verProducto/'.$productos->id; ?> "><button type="button" class="btn btn-block btn-info">Agregar a carrito <i class="fas fa-cart-plus"></i></button></a>

        </div>
      </div>	
    </div>
  <?php  }   ?>

</div>
<div class="collapse" id="collapseExample"> 
  <div class="row">


   <?php foreach ($pro as $produ) {  ?>

    
     <div class="col my-4">
      <div class="card border-light mb-3" style="max-width: 30rem;">

       <div class="card-body">

        <a href="<?php echo base_url().'inicio/verProducto/'.$produ->id; ?>"><img style="border-radius: 10px;" width="200" height="215" src="<?php echo base_url().'img/productos/'.$produ->img; ?>"></a>
        <br><br>
        <div class="text-center"><b><?= $produ->nombre_producto ?></b></div>
        <br>
        <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
        <p class="precio">$<?= $produ->precio ?></p>
        <a href="<?php echo base_url().'inicio/verProducto/'.$produ->id; ?> "><button type="button" class="btn btn-block btn-info">Agregar a carrito <i class="fas fa-cart-plus"></i></button></a>

      </div>
    </div>  
  </div>
<?php  }   ?>

</div>



</div>





<div class="text-right">
  <p>
    <a class="btn btn-link"  data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
     Ver todos <i class="fas fa-chevron-circle-down"></i>
   </a>
 </p>

</div>



</div>


</body>

