<!DOCTYPE html>
<html lang="es">
<head>

	<title>Login</title>
	<!--links-->
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--===============================================================================================-->	
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
	<!--===============================================================================================-->
	<link href="https://stackpath.bootstrapcdn.com/bootswatch/4.4.1/cerulean/bootstrap.min.css" rel="stylesheet" integrity="sha384-LV/SIoc08vbV9CCeAwiz7RJZMI5YntsH8rGov0Y2nysmepqMWVvJqds6y0RaxIXT" crossorigin="anonymous">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href=
	"<?php echo base_url().'p_login/fonts/font-awesome-4.7.0/css/font-awesome.min.css'; ?>">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href=
	"<?php echo base_url().'p_login/fonts/iconic/css/material-design-iconic-font.min.css'; ?>">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'p_login/vendor/animate/animate.css'; ?>">
	<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href=
	"<?php echo base_url().'p_login/vendor/css-hamburgers/hamburgers.min.css'; ?>"
	>
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'p_login/vendor/animsition/css/animsition.min.css'; ?>">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href=
	"<?php echo base_url().'p_login/vendor/select2/select2.min.css'; ?>">
	<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href=
	"<?php echo base_url().'p_login/vendor/daterangepicker/daterangepicker.css'; ?>">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'p_login/css/util.css'; ?>"
	>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'p_login/css/main.css'; ?>">
	<!--===============================================================================================-->
</head>
<body class="fondo" style=" background: url(<?php echo base_url().'p_login/images/compra2.jpg';?>); 
background-size: cover;
background-position: center center;
background-repeat: no-repeat;
background-attachment: fixed;
background-size: cover;
background-color: black;">



<div class="limiter">
	<div class="container-login100">
		<div class="wrap-login100">
			<!--Llamando metodo de controlador para recuperar datos de formulario-->
			<form class="login100-form validate-form" autocomplete="off" action="<?php echo base_url().'login_controller/iniciar'; ?>" method="POST" >
				<span class="login100-form-title p-b-26">
					Tienda Online
				</span>

				<div class="wrap-input100 validate-input" data-validate = "
				El correo electrónico válido es: a@b.c">
				<input class="input100" type="text" name="correo">
				<span class="focus-input100" data-placeholder="Correo"></span>
			</div>

			<div class="wrap-input100 validate-input" data-validate="Campo requerida">
				<input class="input100" type="password" name="clave">
				<span class="focus-input100" data-placeholder="Contraseña"></span>

			</div>

			<div class="btn-centrar">
				<button type="submit" class="btn btn-outline-info btn-block">Iniciar Sesión</button>
				<br>

				<!--Llamando controlador para ingresar al formulario de registro-->
				<span class="text-info">¿Eres nuevo? </span><a href="<?php echo base_url('registro_controller'); ?>"><span class="text-primary">Regístrate ahora</span></a>
				<!--Fin-->
			</div>
			<br>

			<!--Desplegar modal para ingresar un correo de recuperacion-->
			<center><div >
				<a href=""  data-toggle="modal" data-target="#correo"><label class="text-link">¿Olvide mi contraseña?</label></a>
			</div></center>
			<!--Fin de modal-->

		</form>
		<!--Fin de formulario-->
	</div>
</div>
</div>

<!-- Modal para ingresar correo y enviar nueva contraseña al correo -->
<div class="modal fade" id="correo" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-scrollable" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalScrollableTitle">Digite su correo</h5>
				<button type="button"  class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>

			<!--Formulario para ingresar correo de recuperacion-->
			<form action="<?php echo base_url().'sendCorreoController/index' ?>" method="POST">
				<div class="modal-body">
					<label style="color: red">Se enviara un email al correo que especifique!</label>
					<div class="row">
						<div class="col">
							<div class="input-group">
								<span class="input-group-text" ><i class="fa fa-tags" >&nbsp</i>Correo</span>
								<input type="text" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm" name="correo" id="correo">
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
					<button type="submit" class="btn btn-primary">Recuperar cuenta</button>
				</div>
			</form>
			<!--Fin de formulario-->
		</div>
	</div>
</div>
<!-- Fin de modal -->


<div id="dropDownSelect1"></div>

<?php $this->load->view('alerts/alertsLogin') ?>

<!--===============================================================================================-->
<script src="<?php echo base_url().'p_login/vendor/jquery/jquery-3.2.1.min.js'; ?>"
	></script>
	<!--===============================================================================================-->
	<script src="<?php echo base_url().'p_login/vendor/animsition/js/animsition.min.js'; ?>"></script>
	<!--===============================================================================================-->
	<script src="<?php echo base_url().'p_login/vendor/bootstrap/js/popper.js'; ?>"></script>
	<script src="<?php echo base_url().'p_login/vendor/bootstrap/js/bootstrap.min.js'; ?>"
		></script>
		<!--===============================================================================================-->
		<script src="<?php echo base_url().'p_login/vendor/select2/select2.min.js'; ?>"
			></script>
			<!--===============================================================================================-->
			<script src="<?php echo base_url().'p_login/vendor/daterangepicker/moment.min.js'; ?>"></script>
			<script src="<?php echo base_url().'p_login/vendor/daterangepicker/daterangepicker.js'; ?>"
				></script>
				<!--===============================================================================================-->
				<script src="<?php echo base_url().'p_login/vendor/countdowntime/countdowntime.js'; ?>"></script>
				<!--===============================================================================================-->
				<script src="<?php echo base_url().'p_login/js/main.js'; ?>"></script>

			</body>

			<!-- Alerta de correo o contraseña erroneo -->
			<?php if (isset($msj)) { 

				if ($msj=="Error"){ ?>
					<script type="text/javascript">
						Swal.fire({
							position: 'top-end',
							icon: 'error',
							title: 'Error al validar sus credenciales!!',
							text: "Correo o contraseña incorrectos!.",
							showConfirmButton: false,
							timer: 5000
						});
					</script>	
				<?php } ?>
			<?php } ?>
			<!-- Fin de Alerta  -->
			</html>