
<?php $this->load->view('Tem_index/header'); ?>


<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor03" aria-controls="navbarColor03" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarColor03">
    <ul class="navbar-nav mr-auto">
     
      <li class="nav-item active">
        <a href="<?php echo base_url().'inicio'; ?>" class="nav-link">TODAS LAS CATEGORÍAS<span class="sr-only">(current)</span></a>
      </li>
      <?php foreach ($categoria as $categorias) {?>

        <li class="nav-item">
          <a class="nav-link" href="#"><?= $categorias->nombre_cate ?></a>
        </li>
      <?php  } ?>
<!--
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          TODAS LAS CATEGORÍAS
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
           <?php foreach ($categoria as $categorias) {?>
          <a class="dropdown-item" href="#"><?= $categorias->nombre_cate ?></a>
            <?php  } ?>
        </div>
      </li>
    -->
  </ul>
  <?php if ($this->session->userdata('nombre')) { ?>

   <form class="form-inline my-2 my-lg-0">

    <a class="nav-link"><span class="text-info"><?= "Bienvenid@ ".$this->session->userdata('nombre') ?></span></a>
    <a class="nav-link mr-sm-2 btn-outline-info"  style="border-radius: 30px;" href="<?php echo base_url().'login_controller/cerrar'; ?>"><span >CERRAR SESIÓN</span></a>
  </form>
<?php  }else{?>

 <a class="nav-link mr-sm-2" style="border-radius: 30px;" href="<?php echo base_url().'login_controller'; ?>"><span >INICIAR SESIÓN</span></a>
<?php  } ?>

<a href="<?php echo base_url('carrito_controller/index') ?>"><button style="border-radius: 10px;" class="btn-sm btn-info my-2" type="button"><i class="fas fa-shopping-cart"> Compras</i></button></a>
</div>
</nav>

<?php $this->load->view($pagina,$data); ?>

<?php $this->load->view('Tem_index/footer'); ?>

