<footer>
 
  <div class="mt-5 pt-5 pb-5 footer" style="background-color: #565656">
   <div class="container">
    <div class="container-fluid">
     <div class="row">
       <div class="col-lg-3 col-xs-12 categoria">
        <h4 class="mt-lg-0 mt-sm-3 text-white">CATEGORÍAS</h4>
        <ul class="m-0 p-0" style="list-style-type: none;">
         <li ><a href="#" class="text-white">Belleza</a></li>
         <li ><a href="#" class="text-white">Hogar</a></li>
         <li ><a href="#" class="text-white">Tecnología</a></li>
         <li ><a href="#" class="text-white">Jardín</a></li>
         <li ><a href="#" class="text-white">Deporte</a></li>
       </ul>
       
     </div>
     <div class="col-lg-2 col-xs-12 ayuda">
       <h4 class="mt-lg-0 mt-sm-3 text-white">AYUDA</h4>
       <ul class="m-0 p-0" style="list-style-type: none;">
         <li><a href="#" class="text-white">Tu Cuenta</a></li>
         <li><a href="#" class="text-white">Pedidos</a></li>
         <li><a href="#" class="text-white">Tarifario de envio</a></li>
       </ul>
     </div>
     <div class="col-lg-3 col-xs-12 conocenos">
       <h4 class="mt-lg-0 mt-sm-4 text-white">CONÓZCANOS</h4>
       <ul class="m-0 p-0" style="list-style-type: none;">
         <li><a href="#"  class="text-white" >Quienes Somos</a></li>
         <li><a href="#"  class="text-white" >Políticas</a></li>
         <li><a href="#"  class="text-white" >Anuncia con Nosotros</a></li>
         <li><a href="#"  class="text-white" >Promociones</a></li>
       </ul>
     </div>
     <div class="col-lg-3 col-xs-12 comunidad">
       <h4 class="mt-lg-0 mt-sm-4 text-white">NUESTRA COMUNIDAD</h4>
       <p><a href="#" class="text-white"><i class="fab fa-facebook-square fa-lg"></i>  </a><a href="#" class="text-white"><i class="fab fa-instagram fa-lg"></i>  </a><a href="#" class="text-white"><i class="fab fa-youtube fa-lg"></i></a></p>
     </div>
   </div>

 </div>
</div>
<div class="row mt-5 copyright" style="background-color: #3B3B3B">
 <div class="col">
   <p style="text-align: center; font-size: 20px;" class=""><small class="text-white-50">© Derechos reservados 2019.</small></p>
 </div>
</div>
</div>
</footer>

</html>