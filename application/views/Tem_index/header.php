<!DOCTYPE html>
<html>
<head>
	<title><?= $titulo; ?></title>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

	<link href="https://stackpath.bootstrapcdn.com/bootswatch/4.4.1/cerulean/bootstrap.min.css" rel="stylesheet" integrity="sha384-LV/SIoc08vbV9CCeAwiz7RJZMI5YntsH8rGov0Y2nysmepqMWVvJqds6y0RaxIXT" crossorigin="anonymous">

	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js" integrity="sha384-6khuMg9gaYr5AxOqhkVIODVIvm9ynTT5J4V1cfthmT+emCG6yVmEZsRHdxlotUnm" crossorigin="anonymous"></script>
	<script src="https://kit.fontawesome.com/11f5858a75.js" crossorigin="anonymous"></script>

	<link href="https://fonts.googleapis.com/css?family=Oswald|Sulphur+Point&display=swap" rel="stylesheet">

	<link href="https://fonts.googleapis.com/css?family=Sulphur+Point&display=swap" rel="stylesheet">
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
  <script src="<?php echo base_url('bootstrap/js/tarjeta.js'); ?>"></script>
  <script src="<?php echo base_url('bootstrap/js/jquery.mask.min.js') ?>"></script>

	<style type="text/css">


		div.card-header{
			font-family: 'Oswald', sans-serif;
			font-size: 18px;
		}


		a.nav-link{

			font-family: 'Oswald', sans-serif;

		}
		h4.titulo-carousel{
			font-style: oblique;
			font-size: 30px;
			font-weight: 900;
		}
		p.p-carousel{
			font-style: oblique;
			font-weight: 900;
		}

		button.boton{
			font-style: oblique;
			font-weight: 900;
		}
		p.precio{
			font-family: 'Oswald', sans-serif;
		}
		

		h2.titulo-detalles{
			font-family: 'Oswald', sans-serif;
			color: black;
			font-size: 30px;
			
		}

		div.carousel-caption{
			text-align: left;
		}

		h2.card-title{
			font-family: 'Sulphur Point', sans-serif;
			color: black;
		}

		body{
			font-family: 'Sulphur Point', sans-serif;
			color: black;
		}

		#precio{
			font-family: 'Oswald', sans-serif;
		}

		footer{
			font-family: 'Oswald', sans-serif;
		}
	</style>

</head>
