<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class tarjeta_controller extends CI_Controller {

	public function __construct(){
		//se carga el constructor de la clase CI_controller
		parent::__construct();
		//funcion que carga el modelo
		$this->load->model('tarjeta_model');
	}

	//funcion de mostrar
	public function index()
	{//se carga la vista
		$this->load->view('tarjeta_view');
	}//fin de metodo mostrar

	
//metodo de ingresar tarjeta
	public function ingresar(){
		//se recuperan los del formulario
		$datos['numero'] = $_POST['numero'];
		$datos['fecha'] = $_POST['fecha'];
		$datos['titular'] = $_POST['titular'];
		$datos['codigo'] = $_POST['codigo'];
		

		//se mandan los datos recuperados aun metodo llamado set_tarjeta
		$result=$this->tarjeta_model->set_tarjeta($datos);
		if ($result=="success"){

			$id=$this->session->userdata('id');
			$this->tarjeta_model->tarjeta_cliente($datos,$id);
		}
		redirect('carrito_controller/index','refresh');


	}//fin del metodo ingresar tarjeta
}//fin de la clase tarjeta_controller