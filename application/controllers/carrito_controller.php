<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class carrito_controller extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('carrito_model');
		$this->load->model('inicio_model','ini');
	}



	public function index(){
		
		$data = array(
			'titulo' => 'Carrito ' ,
			'pagina' => 'carrito_view',
			'categoria' => $this->ini->categorias(),
			'data' => array());
		$this->load->view('Tem_index/main',$data);
		

	} 

	public function pago(){
		$data = $this->carrito_model->get_pago();
		if($data){
			$pagos = array('id'=> $data->id,'tarjeta' => $data->n_tarjeta, 'codigo'=> $data->codigo_cvv);
			$this->session->set_userdata($pagos);
		}


	}

	public function verificar(){
		$datos['numero']= $_POST['numero'];
		$datos['codigo']= $_POST['codigo'];
		$dat = $this->carrito_model->validar($datos);

		if($dat == 1){
			//$verificar = array('tarjeta' => $data->n_tarjeta, 'codigo'=> $data->codigo_cvv);
		//	$this->session->set_userdata($verificar);
			
			$this->carrito_model->guardar();



		$data = array(
			'titulo' => 'Carrito ' ,
			'msj' => 'exito',
			'pagina' => 'carrito_view',
			'categoria' => $this->ini->categorias(),
			'data' => array());

		$this->load->view('Tem_index/main',$data);

		}else{
		

		$data = array(
			'titulo' => 'Carrito ' ,
			'msj' => 'ErrorM',
			'pagina' => 'carrito_view',
			'categoria' => $this->ini->categorias(),
			'data' => array());

		$this->load->view('Tem_index/main',$data);
		

	} 

			
			///$this->load->view('carrito_view',$data);
			//redirect('carrito_controller/index','refresh');
		}

	

// 	public function comprar(){


// 		$this->carrito_model->guardar();

// 		$data = array(
// 			'title' => 'tienda||carrito',
// 			'data' => array());

// 		$this->load->view('Tem_index/main',$data);

// 	}
}


?>