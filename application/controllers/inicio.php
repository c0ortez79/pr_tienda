<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class inicio extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('inicio_model','ini');
	}

//Carga la pagina principal del aplicativo	
	public function index(){
//Creamos un arreglo para llenar el Template con toda la información
		$data = array('titulo' => 'Inicio' ,
			'pagina' => 'inicio_view',
			'categoria' => $this->ini->categorias(),
			'producto' => $this->ini->productos(),
			'pro' => $this->ini->produ(),
			'data' => array());

		$this->load->view('Tem_index/main',$data);
	}
//Esta funcion nos permite ver los detalles de cada producto por su id
	public function verProducto($id){
		$data = array('titulo' => 'Detalles | Producto' ,
			'pagina' => 'productoDetalles',
			'categoria' => $this->ini->categorias(),
			'detalle' => $this->ini->detallesProducto($id),
			'data' => array());
		$this->load->view('Tem_index/main',$data);
	}

	


}
