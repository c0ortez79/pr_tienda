<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class registro_controller extends CI_Controller {//inico d la clase registro_controller

	public function __construct(){
		//se carga el constructor de la clase CI_controller
		parent::__construct();
		//funcion que carga el modelo
		$this->load->model('registro_model');
	}

	//metodo de mostrar
	public function index()
	{//se carga la vista 
		$this->load->view('registro_view');
	}//fin del metodo mostrar

//metodo de ingresar cliente
	public function ingresar(){
		//se recupera los datos del formulario
		$datos['nombre'] = $_POST['nombre'];
		$datos['apellido'] = $_POST['apellido'];
		$datos['fecha'] = $_POST['fecha'];
		$datos['genero'] = $_POST['genero'];
		$datos['direccion'] = $_POST['direccion'];
		$datos['correo'] = $_POST['correo'];
		$datos['clave'] = md5($_POST['clave']);
//se manda los datos a un metodo llamado set_registro
		$this->registro_model->set_registro($datos);
		redirect('login_controller/index','refresh');

	}//fin del metodo ingresar
}//fin de la clase registro_controller