<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class addcarrito_controller extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('inicio_model','ini');
		$this->load->library('cart');
	}


	public function carrito($id){

		$data  =array(
			'id' => $this->input->post("id"),
			'qty' => $this->input->post("cantidad"),
			'price' => $this->input->post("precio"),
			'name' => $this->input->post("nombre")
		);
		$this->cart->insert($data);


		

		$data = array('titulo' => 'Detalles | Producto' ,
			'pagina' => 'productoDetalles',
			'categoria' => $this->ini->categorias(),
			'detalle' => $this->ini->detallesProducto($id),
			'data' => array());
		$this->load->view('Tem_index/main',$data);
		$this->load->view('productoDetalles');
	}
}

?>
