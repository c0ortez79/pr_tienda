<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class login_controller extends CI_Controller{

	//se crea el metod constructor y luego se carga el modelo
	public function __construct(){
		parent:: __construct();
		$this->load->model('login_model');
	}

	//se crea el metodo index
	public function index(){
		$this->load->view('login_view');
	}


	//se crea el metodo que obtiene los datos traidos desde el formulario
	public function iniciar(){

		if ($this->input->post()) {
			$datos['correo'] = $_POST['correo'];
			$datos['clave'] = md5($_POST['clave']);
			$data = $this->login_model->validar($datos);

			if($data){
			//se crean variables de sesion
				$datos_correo = array('id' => $data->id, 'nombre'=> $data->nombre, 'logueado' => TRUE);

				$this->session->set_userdata($datos_correo);

				redirect('/inicio/index','refresh');
			}else{
				$data['msj'] = "Error";
				$this->load->view('login_view',$data);
			//redirect('login_controller');


				
			}
		}else{
			$this->index();
		}
	}
//Metodo para cerrar sesion y destruir la variable de sesion
	public function cerrar(){
		$user_data = array('Logueado' => FALSE);
		$this->session->set_userdata($user_data);
		$this->session->sess_destroy();
		redirect('inicio','refresh');
	}

	//metodo para llamar las alertas desde la vista 
	public function correo($msj){
		
		if ($msj == "errorCorreo") {
			$data['msj'] = "errorCorreo";
		}elseif ($msj == "okCorreo") {
			$data['msj'] = "okCorreo";
		}else{
			$data['msj'] = "errorEnviar"; 
		}
		
		$this->load->view('login_view',$data);
	}


}